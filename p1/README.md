> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Cy Yates

### Project 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Develop an application that runs a data analysis
3. Include a graph to display data
4. Bitbucket repo links:
	* My bitbucket homepage

#### README.md file should include the following items:

* Screenshot of *Data Analysis* application running
* Screenshot of *Data Analysis Graph* 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Data Analysis running in Visual Studio Code*:

![Screenshot of Data Analysis in VSC:](img/p1.png)

*Screenshot of Data Anlysis Graphic*:

![Screenshot of Data Anlysis Graphic running in IDLE:](img/p1_graph.png)


#### Bitbucket Links:

*Cy's Homepage:*
<https://bitbucket.org/cdy15/lis4369>

