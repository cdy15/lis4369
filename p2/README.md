> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Cy Yates

### Project 2 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Develop an application that runs a data analysis on the mtcars dataframe
3. Include two graphs to display data
4. Bitbucket repo links:
	* My bitbucket homepage

#### README.md file should include the following items:

* Screenshot of *Data Analysis* application running
* Screenshot of *Displacement vs MPG* graph using ggplot
* Screenshot of *Weight vs MPG* using plot

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Data Analysis running in RStudio*:

![Screenshot of Data Analysis in RStudio:](img/r_data.png)

*Screenshot of Displacement vs MPG*:

![Screenshot of Displacement vs MPG:](img/plot_disp_and_mpg_1.png)

*Screenshot of Weight vs MPG*:

![Screenshot of Weight vs MPG:](img/plot_disp_and_mpg_2.png)


#### Bitbucket Links:

*Cy's Homepage:*
<https://bitbucket.org/cdy15/lis4369>

