> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Cy Yates

### Assignment 3 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Develop an application that estimates a painting job; Includes cost of labor
3. Bitbucket repo links:
	* My bitbucket homepage

#### README.md file should include the following items:

* Screenshot of *Painting Estimator* application running

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Painting Estimator running in Visual Studio Code*:

![Screenshot of Painting Estimator in VSC](img/paint_est_vsc.png)

*Screenshot of Painting Estimator running in IDLE*:

![Screenshot of Painting Estimator running in IDLE:](img/paint_est_idle.png)


#### Bitbucket Links:

*Cy's Homepage:*
<https://bitbucket.org/cdy15/lis4369>

