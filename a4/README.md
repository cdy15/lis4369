> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Cy Yates

### Assignment 4 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Develop an application that scrapes data from https://raw.github.com/vincentarelbundock/Rdatasets/master/csv/Stat2Data/Titanic.csv
3. Include a graph to display data
4. Bitbucket repo links:
	* My bitbucket homepage

#### README.md file should include the following items:

* Screenshot of *Data Analysis 2* application running
* Screenshot of *Data Analysis 2 Graph* 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Data Analysis 2 running in Visual Studio Code*:

![Screenshot of Data Analysis in VSC:](img/a4.png)

*Screenshot of Data Anlysis 2 Graphic*:

![Screenshot of Data Anlysis 2 Graphic:](img/a4_graph.png)


#### Bitbucket Links:

*Cy's LIS4369 Homepage:*
<https://bitbucket.org/cdy15/lis4369>

