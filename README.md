> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4369

## Cy Yates

### LIS4369 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
	- Install Python
	- Install R
	- Install R Studio
	- Install Visual Studio Code
	- Create *a1_tip_calculator* application
	- Create Bitbucket repo
	- Complete Bitbucket tutorial (bitbucketstationlocations)
	- Provide git command descriptions
2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Develop Payroll application
	- Application must account for overtime/holiday hours and the overtime/holiday pay rate
	- Provide new git command descriptions
3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Develop an application that estimates painting costs
		- Takes Labor into account
	- Make the applicaiton function recursive
4. [A4 README.md](a4/README.md "My A4 README.md file")
	- Create an application that pulls in data from a specified URL
	- Display data and data types
	- Display graph of data
	- Install needed python packages
5. [A5 README.md](a5/README.md "My A5 README.md file")
	- Completed R: A Beginner's Guide tutorial
	- Execute basic R commands
	- Import data using R
	- Visualize data with charts using R
6. [P1 README.md](p1/README.md "My P1 README.md file")
	- Install Python packages like pip and pandas
	- Create a program that runs an analysis on a set of data
	- Include a graph
7. [P2 README.md](p2/README.md "My P2 README.md file")
	- Import data from mtcars using RStudio
	- Grab and display different columns of the mtcars datframe
	- Inlcude 2 graphs derived from dataset

