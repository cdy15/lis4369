> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Cy Yates

### Assignment 2 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Develop an application that calculates a user's gross pay, including their holiday and overtime hours
3. Bitbucket repo links:
	* My bitbucket homepage

#### README.md file should include the following items:

* Screenshot of *a2_payroll_calculator* application running
* Git commands w/short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1.	git remote -v � List all currently configured remote repositories
2.	git branch � List all the branches in your repo
3.	git grep � Allows you to search file contents 
4.	git diff � View conflicts between two files
5.	git tag � Lists all tags
6.	git status � List the files you've changed and those you still need to commit
7.	git branch -d � Enables you to delete a branch


#### Assignment Screenshots:

*Screenshot of payroll with no overtime running in IDLE*:

![Screenshot payroll with no overtime in IDLE](img/no_ovrtm.png)

*Screenshot of payroll with overtime running in IDLE*:

![Screenshot of payroll with overtime running in IDLE:](img/with_ovrtm.png)


#### Bitbucket Links:

*Cy's Homepage:*
<https://bitbucket.org/cdy15/lis4369>

