> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Cy Yates

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions
4. Bitbucket repo links:
	* this assignment and
	* the completed tutorial (bitbucketstationlocations)

#### README.md file should include the following items:

* Screenshot of *a1_tip_calculator* application running
* Git commands w/short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1.	git init � Allows you to create an empty Git repository 
2.	git status � Displays the state of the directory currently being used
3.	git add � Allows you to add file contents 
4.	git commit � Make changes to the current repository
5.	git push � Allows you to update your remote repositories with your data
6.	git pull � Allows you to grab data from another repository
7.	git checkout � Enables you to switch from one branch to another


#### Assignment Screenshots:

*Screenshot of running Calculator in IDLE*:

![Screenshot of running Calculator in IDLE](img/calc_idle.png)

*Screenshot of running Calculator in Visual Studio Code*:

![Screenshot of running Calculator in Visual Studio Code:](img/calc_vsc.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
<https://bitbucket.org/cdy15/bitbucketstationlocations>

*Assignment 1 - Tip Calculator:*
<https://bitbucket.org/cdy15/lis4369/src/7b03d1f0fff4a98d25f956607f9a54ba7f981966/a1/README.md>

