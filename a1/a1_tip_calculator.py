print("Tip Calculator\n")

print("Program Requirements")
print("1. Must use float data type for user input (except, *Party Number*).")
print("2. Must round calculations to two decimal places.")
print("3. Must format currency with dollar sign, and two decimal places.\n")

print("User Input:")
meal = float(input("Cost of Meal: "))
tax = float(input("Tax Percent:  "))
tip = float(input("Tip Percent:  "))
party = int(input("Party Number: "))

# Format meal input to currency
meal_string = '${:,.2f}'.format(meal)

# Format tax input to currency
tax_total = meal * (tax * 0.01)
tax_string = '${:,.2f}'.format(tax_total)

# Format amount due to currency
amount_due = meal + tax_total
amount_string = '${:,.2f}'.format(amount_due)

# Format tip input to currency
tip_total = amount_due * (tip * 0.01)
tip_string = '${:,.2f}'.format(tip_total)

# Format total cost to currency
total_cost = meal + tax_total + tip_total
total_string = '${:,.2f}'.format(total_cost)

# Split the bill
split = total_cost / party
split_string = '${:,.2f}'.format(split)


print("\nProgram Output:")
print("Subtotal: \t" + meal_string)
print("Tax: \t\t" + tax_string) 
print("Amount Due: \t" + amount_string)
print("Gratuity: \t" + tip_string)
print("Total: \t\t" + total_string)
print("Split (" + str(party) + "): \t" + split_string)

print()