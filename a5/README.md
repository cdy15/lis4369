> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Cy Yates

### Assignment 5 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Develop an application that scrapes data from https://raw.github.com/vincentarelbundock/Rdatasets/master/csv/Stat2Data/Titanic.csv using R
3. Include a graph to display data
4. Bitbucket repo links:
	* My bitbucket homepage
	* Completed R Tutorial

#### README.md file should include the following items:

* Screenshot of *Titanic Passenger Ages* running in RStudio
* Screenshot of *Relationship Between Ages and Survival* running in RStudio

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Distribution of Titanic Passenger Ages running in RStudio*:

![Screenshot of Distribution of Titanic Passenger Ages:](img/a5_ages.png)

*Screenshot of Relationship Between Ages and Survival running in RStudio*:

![Screenshot ofRelationship Between Ages and Survival:](img/a5_survived.png)


#### Bitbucket Links:

*Cy's LIS4369 Homepage:*
<https://bitbucket.org/cdy15/lis4369>

*Cy's r4beginners Homepage:*
<https://bitbucket.org/cdy15/r4beginners>